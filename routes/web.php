<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', ['as' => 'home.index', 'uses' => 'UserController@index']);
Auth::routes();
Route::get('/', ['as' => 'home.index', 'uses' => 'PageController@index']);
Route::get('acerca-de-mi', ['as' => 'home.about', 'uses' => 'PageController@about']);
Route::get('servicios', ['as' => 'home.services', 'uses' => 'PageController@services']);
Route::get('contacto', ['as' => 'home.contact', 'uses' => 'PageController@contact']);
Route::post('contacto', ['as'   => 'home.contact.store', 'uses'   => 'ContactController@store']);
Route::get('blog', ['as' => 'home.blog', 'uses' => 'PageController@blog']);
Route::get('blog-post/{id}', ['as' => 'home.blog_post', 'uses' => 'PageController@blog_post']);
Route::get('service-full/{id}', ['as' => 'home.service', 'uses' => 'PageController@service_full']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin/users', ['as' => 'admin.users', 'uses' => 'UserController@index']);
    Route::get('admin/contacto', ['as' => 'admin.home.contact', 'uses' => 'ContactController@index']);
    Route::get('admin/contacto/delete/{id}', ['as' => 'admin.contacto.destroy', 'uses'   => 'ContactController@destroy']);
    Route::get('admin/testimonios', ['as' => 'admin.home.testimonies', 'uses' => 'TestimonyController@index']);
    Route::get('admin/testimonios/create', ['as' => 'admin.home.testimonies.create', 'uses' => 'TestimonyController@create']);
    Route::post('admin/testimonios/create', ['as' => 'admin.home.testimonies.store', 'uses' => 'TestimonyController@store']);
    Route::get('admin/testimonios/delete/{id}', ['as' => 'admin.testimonies.destroy', 'uses'   => 'TestimonyController@destroy']);

    Route::get('admin/services', ['as' => 'admin.home.services', 'uses' => 'ServiceController@index']);

    Route::get('admin/posts', ['as' => 'admin.home.posts', 'uses' => 'BlogController@index']);
    Route::get('admin/posts/create', ['as' => 'admin.home.posts.create', 'uses' => 'BlogController@create']);
    Route::post('admin/posts/create', ['as' => 'admin.home.posts.store', 'uses' => 'BlogController@store']);
    Route::get('admin/posts/delete/{id}', ['as' => 'admin.posts.destroy', 'uses'   => 'BlogController@destroy']);


    Route::get('admin/events/index/{id}', ['as' => 'admin.home.events', 'uses' => 'EventController@index']);
    Route::get('admin/events/create', ['as' => 'admin.home.events.create', 'uses' => 'EventController@create']);
    Route::post('admin/events/create', ['as' => 'admin.home.events.store', 'uses' => 'EventController@store']);
    Route::get('admin/events/delete/{id}', ['as' => 'admin.events.destroy', 'uses'   => 'EventController@destroy']);


});
