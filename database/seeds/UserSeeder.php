<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'ADMINISTRADOR',
            'email' => 'admin@admin.com',
            'password'     => bcrypt('123456789')
        ]);

        User::create([
            'name' => 'CLIENTE',
            'email' => 'cliente@cliente.com',
            'password'     => bcrypt('123456789')
        ]);

        User::create([
            'name' => 'CUENTAS',
            'email' => 'cuentas@cuentas.com',
            'password'     => bcrypt('123456789')
        ]);

    }
}
