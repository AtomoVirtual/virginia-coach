<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Service::create([
            'img_url'   => 'assets_web/images/coaching.jpg',
            'content'   => 'El coaching es una disciplina de Desarrollo Personal y Profesional y tiene como principal conseguir los objetivos del cliente o coachee y transitar por su proceso de cambio. Desde donde estás hasta dónde quieres estar.

            Trabajaremos creencias limitantes y aprenderemos a Enfocar y Desenfocar las zonas convenientes para tus objetivos y así generar los cambios hacia EL BIENESTAR INTEGRAL.

            Esto ocurre en un espacio de confort, privacidad y total confianza  Como? motivando a los participantes y sacar lo mejor de ellos.
            Consiste en un acompañamiento donde con PREGUNTAS PODEROSAS que realiza el Coach,  llevan al  participante a darse cuenta de su situación y al AUTODESCUBRIMIENTO  y   la AUTOREFLEXION, siempre desde el no juicio, no consejos y total CONFIDENCIALIDAD.

            Sientes la necesidad de un cambio personal y/o profesional ó te sientes confus@ entre varias opciones y no sabes hacia dónde ir?  Te acompaño con el Coaching de Bienestar  que ofrezco para que logres la claridad que requieres, reinventarte, y sentirte más motivad@.

            Trabajo con las ESTRATEGIAS QUE ME HAN FUNCIONADO A MI Y A MIS CLIENTES y la Metodología que más resultados me ha proporcionado.

            Gestionaremos junt@s tus emociones y conseguirás conocerte mejor, mejorar las relaciones personales y profesionales, mayor tranquilidad y BIENESTAR.

            SOLO REQUERIMOS SABER QUIENES SOMOS Y DESCUBRIR JUNT@S QUE LA SALIDA ES HACIA ADENTRO.

            COMO  SE TRABAJA EL COACHING?
            En sesiones donde fijamos objetivos que nos lleven a obtener el resultado deseado.
            Preguntas Poderosas
            Dinámicas de Coaching
            ',
            'title'     => 'Sesiones de Coaching'
        ]);

        Service::create([
            'img_url'   => 'assets_web/images/crp.jpg',
            'content'   => 'CRP es considerado un nuevo estilo de crecimiento personal, avanzado y es sumamente fácil, no requiere labores titánicas, tareas tediosas ni rebuscar en el pasado.

            CRP es de hoy hacia adelante, CRP es evolución, CRP es tener resultados inmediatos.

            El practicante CRP aprende 6 sencillas estrategias que día a día lo conectarán con la felicidad, abundancia y facilidad e irán incrementando su energía o frecuencia vibratoria y el participante aprende a GESTIONAR SUS EMOCIONES  para lograr de esta manera el bienestar personal y la consecución de sus metas personales, económicas, profesionales, emocionales y espirituales.

            Es un programa que empodera a la persona y le permite fluir fácilmente en los diversos ambientes adversos que se le presenten.
            ',
            'title'     => 'Formaciones CRP'
        ]);

        Service::create([
            'img_url'   => 'assets_web/images/piedras.jpg',
            'content'   => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'title'     => 'Talleres de Crecimiento Personal y Espiritual (Online/Presencial)'
        ]);
        Service::create([
            'img_url'   => 'assets_web/images/charla.jpg',
            'content'   => 'De acuerdo a mi experiencia en las distintas áreas del Desarrollo Personal y Espiritual, dicto talleres y conferencias sobre temas diversos e interesantes para que los participantes puedan salir de los mismos siempre con alta vibración y en total BIENESTAR INTEGRAL.  ',
            'title'     => 'Charlas y Conferencias de Crecimiento Personal y Espiritual (Online/Presencial)'
        ]);


    }
}
