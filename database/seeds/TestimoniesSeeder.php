<?php

use Illuminate\Database\Seeder;
use App\Models\Testimony;

class TestimoniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Testimony::create([
            'content'       => 'Con CRP y Virginia Cortiella aprendí que lo que al universo le pides, el universo te da. Paciencia, perseverancia y
            positivismo es imprescindible. Poco a poco mi mapa de los deseos va transformándose en realidad. Ya con trabajo fijo y
            una mudanza prevista para fin de mes estoy muy satisfecha de mis progresos.',
            'client_name'   => 'Sarah Torres'
        ]);

    }
}
