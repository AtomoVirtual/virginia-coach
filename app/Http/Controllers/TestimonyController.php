<?php

namespace App\Http\Controllers;

use App\Models\Testimony;

use Illuminate\Http\Request;

use DB;

class TestimonyController extends Controller
{

    public function index()
    {
        $testimonies = Testimony::all();

        return view('admin.testimonies.index', compact('testimonies'));
    }

    public function create()
    {
        return view('admin.testimonies.create');
    }


    public function store(Request $request)
    {

            $this->validate($request, [
                'client_name' => 'required',
                'content' => 'required',
            ]);

            DB::beginTransaction();

            $testimony = New Testimony();
            $testimony->client_name = $request->client_name;
            $testimony->content = $request->content;
            $testimony->save();

            if (!$testimony) {
                DB::rollBack();
                return redirect()->back()->with('error', 'error');
            }

            DB::commit();
            return redirect()->route('admin.home.testimonies')
            ->with('success','Testimonio Creado con Éxito');

    }


    public function destroy($id)
    {
        DB::table("testimonies")->where('id',$id)->delete();
        return redirect()->route('admin.home.testimonies')
                        ->with('success','Testimonio Eliminado con Éxito');
    }

}
