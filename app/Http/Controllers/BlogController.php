<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use DB;
use Redirect;
use File;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{

    public function index()
    {
        $posts = Post::orderBy('created_at','DESC')->get();

        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(Request $request)
    {

            $this->validate($request, [
                'title' => 'required',
                'img_url' => 'required',
                'content' => 'required',
            ]);

            DB::beginTransaction();

            $post = New Post();
            $post->title = $request->title;
            $post->content = $request->content;

            if ($request->hasfile('img_url')) {
                $file = $request-> file('img_url');
                $filename = time().$file->getClientOriginalName();
                $pos = "img/post_images/";
                $file->move($pos, $filename);
                $post->img_url = $pos.$filename;
            }
            $post->save();
            DB::commit();
            return redirect()->route('admin.home.posts')->with('success','Post Creado con Éxito');

    }


    public function destroy($id)
    {
        DB::table("posts")->where('id',$id)->delete();
        return redirect()->route('admin.home.posts')
                        ->with('success','Post Eliminado con Éxito');
    }

}
