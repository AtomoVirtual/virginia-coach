<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = Contact::all()->sortByDesc('id');
        return view('admin.contactos.index',compact('contacts'));
    }


    public function store(Request $request)
    {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'message' => 'required',
            ]);

            DB::beginTransaction();

            $contact = New Contact();
            $contact->email = $request->email;
            $contact->name = $request->name;
            $contact->message = $request->message;
            $contact->save();

            if (!$contact) {
                DB::rollBack();
                return redirect()->back()->with('error', 'error');
            }

            DB::commit();
            return redirect()->back()->with('success', 'success');

    }



    public function destroy(Request $request, $id)
    {
        DB::table("contacts")->where('id',$id)->delete();
        return redirect()->back()->with('success', 'success');
    }
}
