<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Event;

use Illuminate\Http\Request;

use DB;

class EventController extends Controller
{

    public function index($id)
    {
        $events = Event::where('service_id',$id)->get();

        return view('admin.events.index', compact('events'));
    }


    public function create()
    {
        $services = Service::all();

        return view('admin.events.create', compact('services'));
    }


    public function store(Request $request)
    {

            $this->validate($request, [
                'event_name' => 'required',
                'type' => 'required',
                'date' => 'required',
                'time' => 'required',
                'event_price' => 'required',
                'event_capacity' => 'required',
                'services_select' => 'required',
            ]);

            DB::beginTransaction();

            $evento = New Event();
            $evento->event_name = $request->event_name;
            $evento->price = $request->event_price;
            $evento->service_id = $request->services_select;
            $evento->type = $request->type;
            $evento->date = $request->date;
            $evento->time = $request->time;
            $evento->capacity = $request->event_capacity;

            $evento->status = 1;
            $evento->zoom_link = $request->zoom_link;
            $evento->wp_link = $request->wp_link;
            $evento->save();

            DB::commit();
            return redirect()->route('admin.home.events', ['id' => $evento->service_id])->with('success','Evento Creado con Éxito');

    }

    public function destroy($id)
    {
        $event = Event::where('id',$id)->first();

        DB::table("events")->where('id',$id)->delete();
        return redirect()->route('admin.home.events', ['id' => $event->service_id])->with('success','Evento Eliminado con Éxito');
    }

}
