<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Testimony;
use App\Models\Post;
use App\Models\Service;

class PageController extends Controller
{

    public function index()
    {
        $testimonies = Testimony::all();
        $posts = Post::orderBy('created_at','DESC')->limit(4)->get();

        return view('welcome', compact('testimonies', 'posts'));
    }

    public function about()
    {
        return view('about');
    }

    public function services()
    {
        $services = Service::all();

        return view('services', compact('services'));
    }
    public function contact()
    {
        return view('contact');
    }

    public function blog()
    {

        $posts = Post::orderBy('created_at','DESC')->get();

        return view('blog', compact('posts'));
    }
    public function blog_post(Request $request, $id)
    {
        $post = Post::find($id);
        $posts = Post::inRandomOrder()->limit(2)->get();

        return view('blog_single', compact('post', 'posts'));
    }

    public function service_full($id)
    {
        $service = Service::find($id);

        return view('service', compact('service'));
    }


}
