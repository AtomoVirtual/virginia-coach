<!doctype html>
<html lang="es">
    @include('layouts.head')
<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another"></section>
    <!-- About Section Start -->
    <div id="about-us">
        <div class="container">
            <h3 data-aos="fade-up" data-aos-delay="300">Soy Virginia Albarracin - Coach & Trainer</h3>
            <div>
                <p >Coach de Bienestar-Agente de Transformación: Especialista Procesos de Transformación con Propósito/Despertar del Ser.</p>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="500">
                <div>
                    <img src="{{URL::asset('assets_web/images/about.jpg')}}" alt="about-bg" class="thumbnail image" style="text-align: center;">
                    <br><br><br>
                    <p>
                        Mi Propósito es acompañarte en tus procesos emocionales y apoyarte a sintonizar con  quien realmente <b>ERES</b>, ese SER de luz y Amor que está dentro de cada uno de nosotros, para que puedas alcanzar el <b>BIENESTAR Y LA PLENITUD</b> deseada.
                        <br><br>
                            <b>Acerca de mí te cuento:</b>
                        <br>
                        En paralelo con la Mejora de Procesos Empresariales durante más de 20 años, siempre he tenido inquietud por todo lo relacionado con el Desarrollo Personal y Expansión de la Consciencia, razón por lo que he incorporado a mi vida todo aquello que me hace sentido y lo he transmitido a mis clientes y alumnos durante las últimas 2 décadas.
                        Después de recorrer un largo camino en la profesión de Administración de Empresas empecé a sentir que ese ya no era mi lugar, prefería estar en contacto con las personas y apoyarlas en la Gestión de sus Emociones, por eso entre Talleres, Formaciones y lecturas, empecé en este camino que me ha dado muchas satisfacciones.
                        <br><br>
                            <b>Me formé en varios temas que me apasionan:</b>
                    </p>

                    <ul>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Metafísica.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Reberthing Técnica de Respiración Consciente y Conectada.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Coach Transaccional (AT).</a></span> </li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Coach & Trainer del Círculo de Realización Personal.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Coaching por Valores.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp PNL Practitioner.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Coaching Aprendiendo a Vivir Mejor.</a></span> </li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Soul Communication  (Archivos Akasikos).</a></span></li><br>

                    </ul>
                    <br>
                    <p>
                        Sigo aprendiendo todo aquello que me inspira y apasiona del Desarrollo y Evolución Personal con implicación de la Espiritualidad y transmitiéndolo a mis <b>CLIENTES.</b><br>
                        Soy creyente 100% que los pensamientos y las emociones crean realidades, tema ya demostrado a nivel cuántico.<br>
                        Convencida que <b>ELEVAR TU FRECUENCIA VIBRATORIA</b> (energía) conduce al <b>BIENESTAR</b> que todos deseamos.
                    </p>
                    <br>
                        <h4 data-aos="fade-left" style="color: #a3a3a3;"><center><a  href="https://www.instagram.com/soyvirginiacoach/" target="_blank" > #OBJETIVOBIENESTAR</center></h4>
                    <br>
                    <div class="about">
                        <div class="row">
                            <div class="col-md-12 col-12 button text-center">
                                <a class="btn btn-success" href="{{route('home.services')}}" role="button">Reserva tu Plaza</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Section End -->


    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')
</body>

</html>
