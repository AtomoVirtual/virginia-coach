<!doctype html>
<html lang="es">
    @include('layouts.head')
<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another"></section>
    <!-- About Section Start -->
    <div id="about-us">
        <div class="container">
            <h3 data-aos="fade-up" data-aos-delay="300">{{$service->title}}</h3>

        @if($service->id == 2)
            <div class="row" data-aos="fade-up" data-aos-delay="500">
                <div>
                    <div class="col">
                        <p allign="justify">Sientes la necesidad de un cambio personal y/o profesional? te sientes confusa/o entre varias opciones y no sabes hacia dónde ir?
                            Te acompaño con el <b>Coaching de Bienestar</b> que ofrezco para que logres la claridad que requieres, reinventarte, y sentirte más motivada/o.
                            El coaching es una disciplina de Desarrollo Personal que tiene como propósito conseguir los objetivos del cliente o coachee y transitar por su proceso de cambio.
                            Descubriremos las creencias limitantes y aprenderemos a Enfocar y Desenfocar las zonas convenientes para tus objetivos y así generar los cambios hacia <b>EL BIENESTAR INTEGRAL</b>.
                            Trabajo con las <b>ESTRATEGIAS QUE ME HAN FUNCIONADO A MI Y A MIS CLIENTES</b> y la Metodología que más resultados me ha proporcionado.
                        </p>
                        <br>
                            <h4 data-aos="fade-left" style="color: #a3a3a3;"><center>¡¡¡ Sintonizando con tu Ser !!!</center></h4>
                        <br>

                    <ul>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Autoconocimiento.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Gestión Emocional.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Identificar Saboteadores Internos.</a></span> </li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Toma de Decisiones.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Gestión del Cambio hacia el BIENESTAR INTEGRAL.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Búsqueda de Propósito.</a></span></li><br>
                    </ul>

                    <br>
                    <h5 data-aos="fade-left" ><center> <b>REQUERIMOS SABER QUIENES SOMOS Y DESCUBRIR JUNT@S QUE</b> </h5><br>
                        <h5 data-aos="fade-left" ><center><b>LA SALIDA ES HACIA ADENTRO.</b></center></h5>

                        <br><br>


                        <ul>
                            <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Primera Sesión Gratuita  &nbsp&nbsp&nbsp&nbsp&nbsp 30 Minutos.</a></span></li><br>
                            <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Paquetes de 3 Sesiones   &nbsp&nbsp&nbsp&nbsp&nbsp 45 Minutos.</a></span></li><br>
                            <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Paquetes de 6 Sesiones   &nbsp&nbsp&nbsp&nbsp&nbsp 45 Minutos.</a></span></li><br>
                            <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Paquetes de 10 Sesiones  &nbsp&nbsp&nbsp&nbsp 45 Minutos.</a></span></li><br>
                        </ul>


                        <br>
                        <h4 data-aos="fade-left" style="color: #a3a3a3;"><center><a  href="https://www.instagram.com/soyvirginiacoach/" target="_blank" > #OBJETIVOBIENESTAR</center></h4>
                    <br>
                    <div class="about">
                        <div class="row">
                            <div class="col-md-12 col-12 button text-center">
                                <a class="btn btn-success"href="https://api.whatsapp.com/send?phone=34640054899&text=Hola%20Virginia%20Coach!%20Me%20interesar%C3%ADa%20conocer%20m%C3%A1s%20sobre%20sus%20sesiones." role="button">Pide tu cita</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($service->id == 1)
            <div class="row" data-aos="fade-up" data-aos-delay="500">
                <div>
                    <div class="col">
                        <p allign = "justify">
                            CRP es considerado un nuevo estilo de crecimiento personal, avanzado y es sumamente fácil, no requiere labores titánicas, tareas tediosas ni rebuscar en el pasado.
                            CRP es de hoy hacia adelante, CRP es evolución, CRP es tener resultados inmediatos.
                            El practicante CRP aprende 6 sencillas estrategias que día a día lo conectarán con la felicidad, abundancia y facilidad e irán incrementando su energía o frecuencia vibratoria y el participante aprende a GESTIONAR SUS EMOCIONES  para lograr de esta manera el bienestar personal y la consecución de sus metas personales, económicas, profesionales, emocionales y espirituales.
                            Es un programa que empodera a la persona y le permite fluir fácilmente en los diversos ambientes adversos que se le presenten.<br>
                            Formación de 6 semanas 2 horas semanales, y 6 maravillosas estrategias que te permiten obtener los resultados y metas que tienes, a través de los siguientes aportes:
                        </p>


                    <ul>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Crear y Mantener en  tu VIDA EL BIENESTAR.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Generar alta Motivación y aprender a mantenerla con estrategias personales.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Conseguir las sus metas económicas, profesionales, emocionales y espirituales en forma rápida y sencilla.</a></span> </li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Experimentar el Estado de BIENESTAR.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Mejorar tus relaciones.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Despertar tu conciencia individual y Conectar con tu espiritualidad.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Alcanzar deseos y Proyectar tus sueño.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Emprendeduría.</a></span></li><br>
                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a >&nbsp Mejora de Relaciones.</a></span></li><br>
                    </ul>


                        <br>
                        <h4 data-aos="fade-left" style="color: #a3a3a3;"><center><a  href="https://www.instagram.com/soyvirginiacoach/" target="_blank" > #OBJETIVOBIENESTAR</center></h4>
                    <br>
                    <div class="about">
                        <div class="row">
                            <div class="col-md-12 col-12 button text-center">
                                <a class="btn btn-success"href="https://api.whatsapp.com/send?phone=34640054899&text=Hola%20Virginia%20Coach!%20Me%20interesar%C3%ADa%20conocer%20m%C3%A1s%20sobre%20sus%20sesiones%20CRP." role="button">Reserva tu Plaza</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif






        </div>
    </div>
    <!-- About Section End -->


    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')
</body>

</html>
