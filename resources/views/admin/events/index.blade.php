@extends('admin.layouts.app')

@section('htmlheader_title')
    Eventos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Acciones</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <center>
                    <a  class="btn btn-success" href="{{route('admin.home.services')}}">Regresar</a>
                </center>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif


            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Eventos</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre del Evento</th>
							<th>Fecha y Hora</th>
							<th>Precio</th>
							<th>Modalidad</th>
							<th>Cupos disponibles</th>
                            <th>Acción</th>

                        </tr>
                    </thead>
                    <tbody>
						@foreach ($events as $event)
						<tr>
							<td>{{ $event->event_name }}</td>
							<td>{{ $event->date }}, {{ $event->time }}</td>
							<td>{{ $event->price }}</td>
                            <td>
                                @if($event->type == 0)
                                En Línea
                                @else
                                Presencial
                                @endif
                            </td>
                            <td>{{ $event->capacity }}</td>
							<td>
                                <center>
                                    <form action="{{route('admin.events.destroy', $event->id)}}" method="get" style="display:inline">
                                        <input type="submit" value="Eliminar" class="btn btn-danger">
                                    </form>
                                </center>
                            </td>
                        </tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection

