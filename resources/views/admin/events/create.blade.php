@extends('admin.layouts.app')

@section('htmlheader_title')
    Eventos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')


<!-- mensaje de error -->
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
     @foreach ($errors->all() as $error)
        {{ $error }} <br>
    @endforeach
</div>
@endif


<form action="{{route('admin.home.events.store')}}" method="POST">
    {{ csrf_field() }}
<div class="row">

        <div class="col-md-3">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Acciones</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <center>
                        <button type="submit" class="btn btn-success btn-block">Crear y Finalizar</button>
                        <a class="btn btn-warning btn-block" href="{{ route('admin.home.services')}}"><span class="fa fa-chevron-left"></span> Regresar</a>
                    </center>
                </div>
            </div>

        </div>

        <div class="col-md-9">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Crear Evento</h3>
                </div>
                <div class="card-body">

                    <div class="form-group">
                        <label for="event_name">Nombre del Evento</label>
                        <input name="event_name" type="text" class="form-control" placeholder="Nombre del Evento">
                    </div>


                    <div class="form-group">
                        <label for="services_select">Servicio Asociado</label>
                        <select class="form-control" name="services_select" id="services_select" required>
                            <option value="">Seleccionar un Servicio</option>
                            @foreach($services as $serv)
                                <option value="{{$serv->id}}">{{$serv->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="date">Fecha</label>
                        <input id="date" name="date" type="date" class="form-control" placeholder="Fecha" required>
                        <input type="time" id="time" class="form-control" name="time" min="00:00" max="24:00" required>
                    </div>


                    <div class="form-group">
                        <label for="event_price">Precio</label>
                        <input name="event_price" type="number" class="form-control" placeholder="Precio del Evento">
                    </div>

                    <div class="form-group">
                        <label for="event_capacity">Capacidad</label>
                        <input name="event_capacity" type="number" class="form-control" placeholder="Capacidad del Evento">
                    </div>


                    <div class="form-group">
                        <label for="type">Modalidad</label>
                        <select class="form-control" name="type" id="type" required>
                            <option value="">Seleccionar una Modalidad</option>
                            <option value="0">En línea</option>
                            <option value="1">Presencial</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="wp_link">Link Grupo Whatsapp</label>
                        <input name="wp_link" type="text" class="form-control" placeholder="Link Grupo Whatsapp">
                    </div>


                    <div class="form-group">
                        <label for="zoom_link">Link Zoom</label>
                        <input name="zoom_link" type="text" class="form-control" placeholder="Link Zoom">
                    </div>



                </div>
            </div>


        </div>


</div>
</form>

@endsection
