@extends('admin.layouts.app')

@section('htmlheader_title')
    Servicios
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Acciones</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <center>
                    <a  class="btn btn-success" href="{{route('admin.home.events.create')}}">Crear Evento</a>
                </center>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif


            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Servicios</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Titulo</th>
							<th>Contenido</th>
							<th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach ($services as $service)
						<tr>
							<td>{{ $service->title }}</td>
							<td>{{ $service->content }}</td>
                            <td>
                                <center>
                                    <a  class="btn btn-success" href="{{route('admin.home.events', $service->id )}}">Ver Eventos</a>
                                </center>
                            </td>
                        </tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection

