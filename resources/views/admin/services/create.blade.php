@extends('admin.layouts.app')

@section('htmlheader_title')
    Usuarios
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')


<!-- mensaje de error -->
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
     @foreach ($errors->all() as $error)
        {{ $error }} <br>
    @endforeach
</div>
@endif


<form action="{{route('admin.home.testimonies.store')}}" method="POST">
    {{ csrf_field() }}
<div class="row">

        <div class="col-md-3">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Acciones</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <center>
                        <button type="submit" class="btn btn-success btn-block">Crear y Finalizar</button>
                        <a class="btn btn-warning btn-block" href="{{ route('admin.home.testimonies')}}"><span class="fa fa-chevron-left"></span> Regresar</a>
                    </center>
                </div>
            </div>

        </div>

        <div class="col-md-9">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Crear Testimonio</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="client_name">Nombre</label>
                        <input name="client_name" type="text" class="form-control" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="content">Mensaje</label>
                        <textarea name="content" class="form-control" rows="3" placeholder="Testimonio..."></textarea>
                    </div>
                </div>
            </div>


        </div>


</div>
</form>

@endsection
