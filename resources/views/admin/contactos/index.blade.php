@extends('admin.layouts.app')

@section('htmlheader_title')
    Mensajes Contacto
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif


            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Mensajes Contacto</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre</th>
							<th>Mail</th>
							<th>Mensaje</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($contacts as $contact)
						<tr>
							<td>{{ $contact->name }}</td>
							<td>{{ $contact->email }}</td>
							<td>{{ $contact->message }}</td>
							<td>
                                <center>
                                    <form action="{{route('admin.contacto.destroy', $contact->id)}}" method="get" style="display:inline">
                                        <input type="submit" value="Eliminar" class="btn btn-danger">
                                    </form>
                                </center>
                            </td>
                        </tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection

