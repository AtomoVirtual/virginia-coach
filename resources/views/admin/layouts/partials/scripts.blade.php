<!-- jQuery -->
<script src="{{ asset('assets_adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets_adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets_adminlte/js/adminlte.min.js') }}"></script>

<script src="{{ asset('assets_adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>

<script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });


$(function () {
    // Summernote
    $('.textarea').summernote()
});

  </script>
