<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">

@section('htmlheader')
    @include('admin.layouts.partials.htmlheader')
@show
<meta name="csrf-token" content="{{ csrf_token() }}">

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<style media="screen">
    .ths{
        padding-top:11px;padding-bottom:11px;background-color:#2f3031;color:gray;
    }
    .ths2{
        padding-top:11px;padding-bottom:11px;background-color:#2f3031;color:white;
    }
    .ths3{
        padding-top:11px;padding-bottom:11px;background-color:#666666;color:white;
    }
    .shortenedSelect {
        max-width: 350px;
    }
</style>
<body class="skin-blue sidebar-mini fixed">
<div class="wrapper">

    @include('admin.layouts.partials.mainheader')

    @include('admin.layouts.partials.sidebar')
    @include('admin.layouts.partials.controlsidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('admin.layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    @include('admin.layouts.partials.footer')

    {{-- @include('layouts.partials.footer') --}}

</div><!-- ./wrapper -->

@section('scripts')
    @include('admin.layouts.partials.scripts')
    @show


<script type="text/javascript">


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Shorten select option text if it stretches beyond max-width of select element
$.each($('.shortenedSelect option'), function(key, optionElement) {
    var curText = $(optionElement).text();
    $(this).attr('title', curText);

    // Tip: parseInt('350px', 10) removes the 'px' by forcing parseInt to use a base ten numbering system.
    var lengthToShortenTo = Math.round(parseInt($(this).parent('select').css('max-width'), 10) / 7.3);

    if (curText.length > lengthToShortenTo) {
        $(this).text('... ' + curText.substring((curText.length - lengthToShortenTo), curText.length));
    }
});

// Show full name in tooltip after choosing an option
$('.shortenedSelect').change(function() {
    $(this).attr('title', ($(this).find('option:eq('+$(this).get(0).selectedIndex +')').attr('title')));
});
</script>
<script type="text/javascript">
  var ruta='{{url('/')}}';
</script>
@yield('script')

</body>
</html>
