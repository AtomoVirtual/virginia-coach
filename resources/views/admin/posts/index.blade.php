@extends('admin.layouts.app')

@section('htmlheader_title')
    Posts
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Acciones</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <center>
                    <a  class="btn btn-success" href="{{route('admin.home.posts.create')}}">Crear Post</a>
                </center>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif


            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Posts</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Título</th>
							<th>Imagen</th>
                            <th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($posts as $post)
						<tr>
							<td>{{ $post->title }}</td>
                        <td>
                            <center>
                                <img src="{{URL::asset($post->img_url)}}" width="40%" height="15%"/>
                            </center>
                        </td>
                            <td>
                                <center>
                                    <form action="{{route('admin.posts.destroy', $post->id)}}" method="get" style="display:inline">
                                        <input type="submit" value="Eliminar" class="btn btn-danger">
                                    </form>
                                </center>
                            </td>
                        </tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection

