@extends('admin.layouts.app')

@section('htmlheader_title')
    Posts
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')


<!-- mensaje de error -->
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
     @foreach ($errors->all() as $error)
        {{ $error }} <br>
    @endforeach
</div>
@endif


<form action="{{route('admin.home.posts.store')}}" method="POST"  enctype='multipart/form-data' >
    {{ csrf_field() }}
<div class="row">

        <div class="col-md-3">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Acciones</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <center>
                        <button type="submit" class="btn btn-success btn-block">Crear y Finalizar</button>
                        <a class="btn btn-warning btn-block" href="{{ route('admin.home.posts')}}"><span class="fa fa-chevron-left"></span> Regresar</a>
                    </center>
                </div>
            </div>

        </div>

        <div class="col-md-9">

            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Crear Post</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Título</label>
                        <input name="title" type="text" class="form-control" placeholder="Título">
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="img_url" name="img_url">
                        <label class="custom-file-label" for="img_url">Subir Imagen</label>
                    </div>
                    <div class="form-group">
                        <label for="content">Contenido</label>
                        <textarea name="content" class="textarea" placeholder="Place some text here"
                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                </div>
            </div>


        </div>


</div>
</form>

@endsection
