<!doctype html>
<html lang="es">
    @include('layouts.head')

<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another"></section>
    <!-- About Section Start -->
    <div id="blog_single">
        <div class="container">
            <h3 data-aos="fade-up" data-aos-delay="300">{{$post->title}} </h3>
            <div class="inner-text" data-aos="fade-up" data-aos-delay="400">
                <h4>{{$post->created_at}} Posteado por: <span><a href="{{route('home.index')}}">Virginia Albarracín</a></span></h4>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="600">
                <div>
                    <img src="{{URL::asset($post->img_url)}}" alt="about-bg" class="thumbnail image">
                    <div class="row">
                        <div class="container">
                            {!!$post->content!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Section End -->
    <section class="blog-page-another">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 heading" data-aos="fade-up" data-aos-delay="300">
                    <h2>Artículos Similares</h2>
                </div>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="400">
                <div class="col-md-12 col-12">
                    <div class="row">
                        @foreach($posts as $post)
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure>
                                        <a href="{{route('home.blog_post', $post->id)}}"><img src="{{URL::asset($post->img_url)}}"  width="304" height="228"></a>
                                    </figure>
                                </div>
                                <div class="col-md-8 inner-content">
                                    <h4><a href="{{route('home.blog_post', $post->id)}}">{!!$post->title!!}</a></h4>
                                    <p><span>{{$post->created_at}}</span>Posteado por: <b><a href="{{route('home.index')}}">Virginia Albarracín</a></b></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer section start-->
    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')
</body>

</html>
