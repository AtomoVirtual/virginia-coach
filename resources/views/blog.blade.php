<!doctype html>
<html lang="es">
    @include('layouts.head')

<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another ">
        <!-- Banner section Start-->
    </section>
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 heading" data-aos="fade-up" data-aos-delay="300">
                    <img src="{{URL::asset('assets_web/images/leaf.png')}}" alt="">
                    <h2>Disfruta de grandiosos Articulos!</h2>
                    <h3>Este es un poco del cocimiento que les quiero compartir</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-12" data-aos="fade-up" data-aos-delay="400">
                    <div class="row">
                        @foreach($posts as $post)
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure>
                                        <a href="{{route('home.blog_post', $post->id )}}"><img src="{{URL::asset($post->img_url)}}"  width="304" height="228"></a>
                                    </figure>
                                </div>
                                <div class="col-md-8 inner-content">
                                    <h4><a href="{{route('home.blog_post', $post->id)}}">{{$post->title}}</a></h4>
                                    <p><span>{{$post->created_at}}</span>Posteado por:  <b><a href="{{route('home.index')}}">Virginia C. Albarracín</a></b></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>


        </div>
    </section>
    <!-- Footer section start-->
    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')
</body>

</html>
