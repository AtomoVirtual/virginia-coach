<!doctype html>
<html lang="es">
    @include('layouts.head')
    <style>

        .carousel-control-prev-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
        }

        .carousel-control-next-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
        }


        .carousel-inner > .item {
            position: relative;
            display: none;
            -webkit-transition: 0.6s ease-in-out left;
            -moz-transition: 0.6s ease-in-out left;
            -o-transition: 0.6s ease-in-out left;
            transition: 0.6s ease-in-out left;
        }

        .carousel-inner .carousel-item {
            transition: -webkit-transform 2s ease;
            transition: transform 2s ease;
        }

    </style>

<body>

    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->
    <!-- Banner section Start -->
    <section class="banner-home">
        <!-- Gradient -->
        <div ></div>
        <!-- container Start-->
        <div class="container">
            <!--Row Start-->
            <div class="row">
                <div class="col-sm-12">
                    <h1 data-aos="fade-left">Descubre todo tu potencial!</h1>
                    <h2 data-aos="fade-left" data-aos-delay="100">Te brindo el mejor estilo de crecimiento personal...</h2>
                    <p data-aos="fade-left" data-aos-delay="300"><i class="fa fa-phone-square" aria-hidden="true"></i><span>Llama para descuentos especiales</span></p>
                    <p data-aos="fade-left" data-aos-delay="400"><i class="fa fa-envelope" aria-hidden="true"></i><span>info@virginialbarracin.com</span></p>
                    <a data-aos="fade-left" data-aos-delay="500" class="btn btn-success" href="{{route('home.about')}}" role="button">Quién Soy</a>
                </div>
            </div>
            <!--Row Ended-->
        </div>
        <!-- container Ended-->
    </section>
    <!-- Banner section Ended -->
    <!-- About section start-->
    <section class="about">
        <!-- container Start-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 mt-5 heading">
                    <img src="{{URL::asset('assets_web/images/leaf.png')}}" alt="">
                    <h2>Las mejores Sesiones de Coaching</h2>
                    <h3>¿Te interesa tener los mejores resultados?</h3>
                </div>
            </div>
            <div class="row" style="text-align: justify;">
                <div class="col">
                    <p allign="justify">Sientes la necesidad de un cambio personal y/o profesional? te sientes confusa/o entre varias opciones y no sabes hacia dónde ir?
                        Te acompaño con el <b>Coaching de Bienestar</b> que ofrezco para que logres la claridad que requieres, reinventarte, y sentirte más motivada/o.
                        El coaching es una disciplina de Desarrollo Personal que tiene como propósito conseguir los objetivos del cliente o coachee y transitar por su proceso de cambio.
                        Descubriremos las creencias limitantes y aprenderemos a Enfocar y Desenfocar las zonas convenientes para tus objetivos y así generar los cambios hacia <b>EL BIENESTAR INTEGRAL</b>.
                        Trabajo con las <b>ESTRATEGIAS QUE ME HAN FUNCIONADO A MI Y A MIS CLIENTES</b> y la Metodología que más resultados me ha proporcionado.
                    </p>
                    <br>
                        <h4 data-aos="fade-left" style="color: #a3a3a3;"><center>¡¡¡ Sintonizando con tu ser !!!</center></h4>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-12 button text-center">
                    <a class="btn btn-success" href="{{route('home.services')}}" role="button">Empezar el Cambio</a>
                </div>
            </div>
        </div>
        <!-- container Ended-->
    </section>
    <!-- About section Ended-->
    <!-- Services section start-->
    <section class="services">
        <!-- container-fluid Start-->
        <div class="container-fluid">
            <div class="row" data-aos="fade-up" data-aos-duration="400">
                <div class="col-md-3">
                    <figure>
                        <img src="{{URL::asset('assets_web/images/services-bg.jpg')}}" alt="The Pulpit Rock">
                    </figure>
                </div>
                <div class="col-md-9 right-part">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>¿Qué Necesitas?</h2>
                            <h3>Te ayudo a crecer en distintos ambitos</h3>
                            <p>Te preguntaras ¿Porque tanto Crecimiento? porque las personas estan obteniendo <b>RESULTADOS INCREIBLES.</b> </p>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xl-5 col-lg-6 col-md-6 col-12 contant-part-1">
                                    <ul>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Alcanzar alta Motivación</a></span></li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Atrae y logra tus Metas</a></span></li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Aprende a crear circunstancias.</a></span> </li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Despertar tu Consciencia y Conectar con tu Ser.</a></span></li>
                                    </ul>
                                </div>
                                <div class="col-xl-5 col-lg-6 col-md-6 col-12 contant-part-2">
                                    <ul>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Conectar con Abundancia.</a></span></li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Aprende a como prosperar.</a></span></li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Transforma tu vida en Felicidad.</a></span> </li>
                                        <li><i class="fas fa-feather-alt" aria-hidden="true"></i><span><a href="{{route('home.services')}}">Que esperas para Empezar?</a></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-12 button text-center">
                                    <a class="btn btn-success" href="{{route('home.services')}}" role="button">Reserva tu Plaza</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- container-fluid Ended-->
    </section>
    <!-- Services section Ended-->
    <!-- Section-4 section start-->
    <section class="section-4">
        <!-- container-fluid Start-->
        <div class="container-fluid">
            <div class="row" data-aos="fade-up"  data-aos-duration="400">
                <div class="col-md-9 right-part">
                    <div class="row">
                        <div class="col-md-12 heading">
                            <br><br><br>
                            <center>
                                <h2>Casos de éxito Increibles!</h2>
                                <h3>Lo mejor es el crecimiento de todos nuestros clientes</h3>
                            </center>
                        </div>
                        @php
                            $p=1;
                        @endphp

                        <div class="col-md-12 mt-3">
                            <div class="row">
                                <div class="col-12">
                                    <br>
                                <center>
                                    <div class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class="align-items-center justify-content-center ">
                                                  <h2>{{$testimonies[0]->client_name}}</h2><br>
                                                  <p>{{$testimonies[0]->content}}</p>
                                                </div>
                                            </div>

                                            @foreach ($testimonies as $testimony)
                                             <div class="carousel-item">
                                                <div class="align-items-center justify-content-center">
                                                  <h2>{{$testimony->client_name}}</h2><br>
                                                  <p>{{$testimony->content}}</p>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </center>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-3">
                    <figure>
                        <img src="{{URL::asset('assets_web/images/section-4-bg.jpg')}}" alt="The Pulpit Rock">
                    </figure>
                </div>
            </div>
        </div>
        <!-- container-fluid Ended-->
    </section>
    <!-- Section-4 section Ended-->
    <!-- Blog section start-->
    <section class="blog">
        <!-- container Start-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 heading">
                    <img src="{{URL::asset('assets_web/images/leaf.png')}}" alt="">
                    <h2>Disfruta de grandiosos Articulos!</h2>
                    <h3>Este es un poco del cocimiento que les quiero compartir</h3>
                </div>
            </div>
            <div class="row" data-aos="fade-up" data-aos-duration="400">
                <div class="col-md-12 col-12">
                    <div class="row">
                        @foreach($posts as $post)
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure>
                                        <a href="{{route('home.blog_post', $post->id )}}"><img src="{{URL::asset($post->img_url)}}"  width="304" height="228"></a>
                                    </figure>
                                </div>
                                <div class="col-md-8 inner-content">
                                    <h4><a href="{{route('home.blog_post', $post->id)}}">{{$post->title}}</a></h4>
                                    <p><span>{{$post->created_at}}</span>Posteado por:  <b><a href="{{route('home.index')}}">Virginia C. Albarracín</a></b></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-12 button">
                    <a class="btn btn-success" href="{{route('home.blog')}}" role="button">Ver Más Artículos</a>
                </div>
            </div>
        </div>
        <!-- container Ended-->
    </section>
    <section class="section-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="col-md-12 heading">
                        <br><br>
                        <center>
                            <h2>Subscribete a mi Newsletter y recibiras un regalo!</h2>
                        </center><br>
                    </div>
                    <form  action="{{route('home.contact.store')}}" method="POST">
                        {{ csrf_field() }}

                        <div style="display: flex; justify-content: center;">
                            <input class="form-control" name="mail_gift" id="mail_gift" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" placeholder="E-mail" required>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                        <br>
                    </form>

                </div>
            </div>
        </div>
    </section>
        <!-- Blog section Ended-->
    <!-- Footer section start-->
    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')


</body>

</html>
