<!doctype html>
<html lang="es">
    @include('layouts.head')

<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another ">
        <!-- Banner section Start-->
    </section>
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 heading" data-aos="fade-up" data-aos-delay="300">
                    <img src="{{URL::asset('assets_web/images/leaf.png')}}" alt="">
                    <h2>Te estamos esperando!</h2>
                    <h3>Responderemos al instante cualquier duda</h3>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer section start-->
    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')

</body>

</html>
