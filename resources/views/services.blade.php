<!doctype html>
<html lang="en">
    @include('layouts.head')
<body>
    <!-- Header section Start -->
    @include('layouts.header')
    <!-- Header section Ended-->

    <section class="banner-another ">
        <!-- Banner section Start-->
    </section>
    <!-- services section Start -->
    <section id="services">
        <div class="container">

            <h2 data-aos="fade-up" data-aos-delay="200">Servicios</h2>


            <div class="part-1" data-aos="fade-up" data-aos-delay="400">

                <p allign = "justify">
                    Primera sesión 30 min.  Gratuita.<br>
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=34640054899&text=Hola%20Virginia%20Coach!%20Me%20interesar%C3%ADa%20conocer%20m%C3%A1s%20sobre%20sus%20servicios."><span style="color: #a3a3a3;">WAPP: +34 640 05 48 99.</span></a>
                </p><br>


                <div class="part-2" data-aos="fade-up" data-aos-delay="400">
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h3><i class="fas fa-feather-alt" aria-hidden="true"></i><a href="{{route('home.service', $services[1]->id)}}" target="_blank">{{$services[1]->title}}</a></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col1">
                            <p allign="justify">Sientes la necesidad de un cambio personal y/o profesional? te sientes confusa/o entre varias opciones y no sabes hacia dónde ir?
                                Te acompaño con el <b>Coaching de Bienestar</b> que ofrezco para que logres la claridad que requieres, reinventarte, y sentirte más motivada/o.<br>
                                Trabajo con las <b>ESTRATEGIAS QUE ME HAN FUNCIONADO A MI Y A MIS CLIENTES</b> y la Metodología que más resultados me ha proporcionado, Sintonizando con tu Ser!
                            </p>
                            <br>
                            <h5 data-aos="fade-left" ><center> <b>REQUERIMOS SABER QUIENES SOMOS Y DESCUBRIR JUNT@S QUE</b> </h5><br>
                                <h5 data-aos="fade-left" ><center><b>LA SALIDA ES HACIA ADENTRO.</b></center></h5>
                        <br>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <figure>
                                <img src="{{URL::asset($services[1]->img_url)}}" alt="Coaching" width="100%">
                            </figure>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12 heading">
                        <h3><i class="fas fa-feather-alt" aria-hidden="true"></i><a href="{{route('home.service', $services[0]->id)}}" target="_blank">{{$services[0]->title}}</a></h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <figure>
                            <img src="{{URL::asset($services[0]->img_url)}}" alt="CRP" width="100%">
                        </figure>
                    </div>
                    <div class="col-md-8 col-sm-6 col-12 col1">
                        <p allign = "justify">
                            CRP es considerado un nuevo estilo de crecimiento personal, avanzado y es sumamente fácil, no requiere labores titánicas, tareas tediosas ni rebuscar en el pasado.
                            CRP es de hoy hacia adelante, CRP es evolución, CRP es tener resultados inmediatos.
                            El practicante CRP aprende 6 sencillas estrategias que día a día lo conectarán con la felicidad, abundancia y facilidad e irán incrementando su energía o frecuencia vibratoria y el participante aprende a GESTIONAR SUS EMOCIONES  para lograr de esta manera el bienestar personal y la consecución de sus metas personales, económicas, profesionales, emocionales y espirituales.
                            Es un programa que empodera a la persona y le permite fluir fácilmente en los diversos ambientes adversos que se le presenten.
                        </p>
                    </div>
                </div>
            </div>

            <div class="part-2" data-aos="fade-up" data-aos-delay="400">
                <div class="row">
                    <div class="col-md-12 heading">
                        <h3><i class="fas fa-feather-alt" aria-hidden="true"></i>{{$services[2]->title}}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <figure>
                            <img src="{{URL::asset($services[2]->img_url)}}" alt="Talleres" width="100%">
                        </figure>
                    </div>
                    <div class="col-md-8 col-sm-6 col1">
                        <br><br><br><p>
                            De acuerdo a mi experiencia en las distintas áreas del Desarrollo Personal y Espiritual, dicto talleres y conferencias sobre temas diversos e interesantes para que los participantes puedan salir de los mismos siempre con alta vibración y en total BIENESTAR INTEGRAL.
                        </p>
                    </div>
                </div>
            </div>
            <!--
            <div class="part-2" data-aos="fade-up" data-aos-delay="400">
                <div class="row">
                    <div class="col-md-12 heading">
                        <h3><i class="fas fa-feather-alt" aria-hidden="true"></i>LECTURA DE REGISTROS AKASIKOS (Proximamente...)</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-6 col1">
                        <p>
                            Los Registros Akáshicos (Soul Communication), guardan la información de cada una de las almas a lo largo de la historia. Su acceso parecía haber sido restringido hasta ahora a unos cuantos seres especiales.
                            Hoy en día parece que la Humanidad necesita más que nunca acceder a esta información para que pueda dejar de depender de voces externas y se guíe solo por la verdad interior, por el Maestro interno.
                            Este curso que tomé, actualmente llamado Soul Communication, está dirigido a aquellos que quieran acceder a su información álmica sin intermediarios, para poder conectar con su camino, vocación o misión todo el tiempo, con la certeza de que la voz interna es la correcta y, además, con la capacidad de acceder a la información de almas cercanas, siempre para el beneficio de todos.
                            Aprendí a escuchar la voz del Alma, para que se convierta en nuestra brújula interna y así poder cumplir nuestro Propósito en la Vida día a día.
                            Realizo la lectura de los archivos que mis clientes me soliciten con todo el respeto y la confidencialidad requerida.
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <figure>
                            <br><br>
                            <img src="{{URL::asset('/assets_web/images/aka.jpg')}}" alt="Coaching" width="100%">
                        </figure>
                    </div>
                </div>
            </div>
            -->
        </div>
    </section>
    <!-- services section Ended -->
    @include('layouts.footer')
    <!-- Footer section Ended-->
    <!-- Return to Top -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layouts.scripts')
</body>

</html>
