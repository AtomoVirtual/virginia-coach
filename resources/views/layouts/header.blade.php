<header class="top">
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a class="active" href="{{route('home.index')}}">Principal</a>
        <a href="{{route('home.about')}}">Conóceme</a>
        <a href="{{route('home.services')}}">Servicios</a>
        <a href="{{route('home.blog')}}">Blog</a>
        <a href="{{route('home.contact')}}">Contacto</a>
    </div>
    <!-- Nav section Start -->
    <nav id="navbar">
        <!-- container Start-->
        <div class="container">
            <!--Row Start-->
            <div class="row">
                <div class="col-lg-4 col-md-4 align-self-center left-side">
                    <p>¿Quieres comenzar? <a target="_blank" href="https://api.whatsapp.com/send?phone=34640054899&text=Hola%20Virginia%20Coach!%20Me%20interesar%C3%ADa%20conocer%20m%C3%A1s%20sobre%20sus%20servicios."><span>tlfn: +34 640 05 48 99</span></a> </p>
                </div>
                <div class="col-lg-3 col-md-3 col-5 align-self-center logo">
                    <a href="{{route('home.index')}}"><img src="{{URL::asset('assets_web/images/nav-logo.png')}}" alt="logo"></a>
                </div>
                <div class="col-lg-5 col-md-5 col-7 align-self-center right-side">
                    <div class="social-icons square">
                        <!-- Page Content -->
                        <div id="page-content-wrapper">
                            <span class="slide-menu" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="social-icons another">
                        <a href="https://www.instagram.com/soyvirginiacoach/" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.facebook.com/soyvirginiacoach" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.linkedin.com/in/virginia-c-albarracin-6a623729/" target="_blank">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>

                    </div>
                </div>
            </div>
            <!--Row Ended-->
        </div>
        <!-- container Ended-->
    </nav>
    <!-- Nav section Ended -->
    <img class="border-img" src="{{URL::asset('assets_web/images/border.png')}}" width="100%" alt="">
</header>
