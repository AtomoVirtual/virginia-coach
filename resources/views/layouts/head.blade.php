<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Virginia Albarracín</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800|Old+Standard+TT:400,400i,700" rel="stylesheet">
    <link rel="shortcut icon" href="{{URL::asset('assets_web/images/mekaba.png')}}" type="image/x-icon">
    <script src="https://kit.fontawesome.com/6f50ded3ca.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/animate.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/main.css')}}">
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{URL::asset('assets_web/images/mekaba.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</head>
