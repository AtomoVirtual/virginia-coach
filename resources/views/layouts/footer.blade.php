@include('layouts.modals')
<footer class="contact">
    <!-- Gradient -->
    <div class="gradient"></div>
    <!-- container Start-->
    <div class="container">
        <div class="row" data-aos="fade-up" data-aos-duration="400">
            <div class="col-lg-6 col-md-12 col-12 columns-1">
                <h2>Dirección</h2>
                <address>
                <p>Barcelona-España</p>
                <p>Llama al: <a target="_blank" href="https://api.whatsapp.com/send?phone=34640054899&text=Hola%20Virginia%20Coach!%20Me%20interesar%C3%ADa%20conocer%20m%C3%A1s%20sobre%20sus%20servicios."><span> +34 640 05 48 99</span></a></p>
                <p>Escribe al: <span>info@virginialbarracin.com</span></p>
               </address>
            </div>
            <div class="col-lg-1 col-md-12 col-12"></div>
            <div class="col-lg-5 col-md-12 col-12 columns-2">
                <h2>Contáctame</h2>
                <form class="row form-inline" action="{{route('home.contact.store')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-md-6 form-group">
                        <input class="form-control" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre'" placeholder="Nombre" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" placeholder="Email" required>
                    </div>
                    <div class="col-md-12 form-group">
                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mensaje'" placeholder=" Mensaje" required></textarea>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- container Ended-->
    <div class="copyright">
        <div class="container">
            <div class="row border-img">
                <div class="col-md-12">
                    <img src="{{URL::asset('assets_web/images/border.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" data-aos="fade-up" data-aos-duration="400">
                <div class="col-lg-3 col-md-12">
                    <a href="{{route('home.index')}}"><img src="{{URL::asset('assets_web/images/footer-logo-bg.png')}}" alt="logo"></a>
                </div>
                <div class="col-lg-9 col-md-12 right-part">
                    <ul class="ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link active" href="{{route('home.index')}}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li><a class="hidden-xs">~</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('home.about')}}">Conoceme</a></li>
                        <li><a class="hidden-xs">~</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('home.services')}}">Servicios</a></li>
                        <li><a class="hidden-xs">~</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('home.blog')}}">Blog</a></li>
                        <li><a class="hidden-xs">~</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('home.contact')}}">Contacto</a></li>
                    </ul>
                    <p>(C) 2020 Todos los Derechos reservados | Hecho con ❤ por <a href="https://www.atomotecnovirtual.com" target="_blank">Átomo Virtual</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<a href="javascript:" id="return-to-top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
